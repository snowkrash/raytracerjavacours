package thing;

import tracer.Intersection;
import tracer.Ray;
import geometry.*;

import java.util.Optional;

public class Sphere {

    double radius;
    Vector center;

    public Sphere(double radius, Vector center) {
        this.radius = radius;
        this.center = center;
    }

    public Optional<Intersection> intersect(Ray ray){
        Vector minusCenter= ray.getPosition().add(new Vector(-center.getX(),-center.getY(),-center.getZ(),0));
        double b=2*minusCenter.dotProduct(ray.getDirection());
        double a=ray.getDirection().getSqareNorme();
        double c=(minusCenter.getSqareNorme()-radius*radius);
        double delta = b*b-4*a*c;
        if (delta<0) return Optional.empty();                   //pas de solutions
        if (Math.max(
                (-b+Math.sqrt(delta))/(2*a),
                (-b-Math.sqrt(delta))/(2*a)
        )<0)
            return Optional.empty();                           // pas de solutions >0
        double min=Math.min(
                (-b+Math.sqrt(delta))/(2*a),
                (-b-Math.sqrt(delta))/(2*a)
        );
        if (min>0)
            return Optional.of(new Intersection(ray,min));//solution = + petite solution >0
        return Optional.of(new Intersection(ray,Math.max(       //si ce n'est toi c'est donc ton frere
                        (-b+Math.sqrt(delta))/(2*a),
                        (-b-Math.sqrt(delta))/(2*a)      )));
    }
}

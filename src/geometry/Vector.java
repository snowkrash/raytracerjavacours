package geometry;

public class Vector {
    double x,y,z;
    int isPoint;

    public Vector(double x, double y, double z, int isPoint) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.isPoint = isPoint;
    }

    public static Vector point(double x, double y, double z){
        return new Vector(x,y,z,1);
    }

    public static Vector vector(double x, double y, double z){
        return new Vector(x,y,z,0);
    }

    public double dotProduct(Vector vec){
        return x*vec.x+y*vec.y+z*vec.z;
    }


    //adition de vecteurs n'as de sens que si les deux sont vecteurs et pas des points
    public Vector add(Vector vec){
        return new Vector(x+vec.x,y+vec.y,z+vec.z, 0);
    }

    private double normalizeCoordinate(double coord, double norme){
        return Math.sqrt((coord*coord)/(norme*norme));
    }

    public Vector normalize(){
        double norme=getNorme();
        return new Vector(
                normalizeCoordinate(x,norme),
                normalizeCoordinate(y,norme),
                normalizeCoordinate(z,norme),
                0);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public int getIsPoint() {
        return isPoint;
    }

    public double getSqareNorme(){return x*x+y*y+z*z; }

    public double getNorme(){
        return Math.sqrt(this.getSqareNorme());
    }


}

package tracer;

import geometry.Vector;
import viewer.Main;

public class Camera {
    Vector position=new Vector(0,0,0,1);
    Vector direction=new Vector(0,1,0,0);

    public Vector getPosition() {
        return position;
    }

    public Ray getCameraToPixelRay(int x,int y){

        return new Ray(getPosition(),getDirectionPixelNormalized(x,y));
    }

    public Vector getDirectionPixel(int x, int y){
        double vectorY=1;//pixel est sur le plan
        double vectorX=x-(Main.IMAGE_WIDTH/2);
        double vectorZ=y-(Main.IMAGE_HEIGHT/2);
        return Vector.vector(vectorX,vectorY,vectorZ);
    }
    public Vector getDirectionPixelNormalized(int x, int y){
        return getDirectionPixel(x, y).normalize();
    }

}

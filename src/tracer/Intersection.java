package tracer;

public class Intersection {
    Ray rayon;
    double distance;

    public Ray getRayon() {
        return rayon;
    }

    public double getDistance() {
        return distance;
    }

    public Intersection(Ray rayon, double distance) {
        this.rayon = rayon;
        this.distance = distance;
    }
}

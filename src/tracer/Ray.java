package tracer;

import geometry.Vector;


public class Ray {
    public Vector getPosition() {
        return position;
    }

    public Vector getDirection() {
        return direction;
    }

    Vector position;
    Vector direction;

    public Ray(Vector position, Vector direction) {
        this.position = position;
        this.direction = direction;
    }

    public Ray ray(int x, int y){
        Camera camera=new Camera();
        Vector rayPosition=new Vector(0,0,0,1);
        Vector rayDirection= camera.getDirectionPixelNormalized(x,y);
        return new Ray(rayPosition,rayDirection);
    }
}

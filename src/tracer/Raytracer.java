package tracer;


import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import viewer.Main;

public class Raytracer {

    public int width = Main.IMAGE_WIDTH;
    public int heigth = Main.IMAGE_HEIGHT;

    final static Color defaultColor=Color.BLACK;
    final Color[][] pixels= new Color[heigth][width];

    /*
    public Raytracer() {

        this(defaultColor);
    } */

    /*      //rays
    public Raytracer() {
      for (int y=0; y< this.getHeigth(); y++)
            for(int x=0; x<this.getWidth();x++){
        this.setColor(x,y, (y/5)%2==0?Color.BLACK:Color.WHITE);
        }
    }*/

    /*      //damier
    public Raytracer(){
        for (int y=0; y< this.getHeigth(); y++)
            for(int x=0; x<this.getWidth();x++){
                this.setColor(x,y, ( (y/5+x/5)%2==0 )?Color.BLACK:Color.WHITE);

            }
    }
    */
/*
    public Raytracer() {
        Camera camera=new Camera();
        for (int y=0; y<heigth; y++)
            for(int x=0; x<width;x++){
                double value= camera.getDirectionPixelNormalized(x,y).dotProduct(camera.direction);
                Color color=new Color(value,value,value,1);
                this.setColor(x,y,color);
            }
    }*/

    public Raytracer(Scene scene) {
        Camera camera=new Camera();
        for (int y=0; y<heigth; y++)
            for(int x=0; x<width;x++){
                Color color=scene.thing.intersect(camera.getCameraToPixelRay(x,y)).isPresent()?Color.WHITE:Color.BLACK ;
                this.setColor(x,y,color);
            }
    }

    public Raytracer(Color color){
        for (int y=0; y<heigth; y++)
            for(int x=0; x<width;x++)
                this.setColor(x,y,color);
    }

    public void setColor(int x, int y, Color color) {
        pixels[y][x]=color;
    }

    public Paint getColor(int x, int y) {
        return pixels[y][x];
  }
}
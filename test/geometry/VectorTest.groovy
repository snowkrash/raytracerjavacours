package geometry

import org.junit.Test

import static org.junit.Assert.*;


class VectorTest  {

    Vector point=Vector.point(1,2,3);

    Vector vector=Vector.vector(1,2,3);
    Vector vectorAdd=Vector.vector(1,1,1)

    @Test
    void testPoint() {
        assertEquals(1,point.x, 1e-9);
        assertEquals(2,point.y, 1e-9);
        assertEquals(3,point.z, 1e-9);
        assertEquals(1,point.isPoint);
    }

    @Test
    void testVector() {
        assertEquals(1,vector.x, 1e-9);
        assertEquals(2,vector.y, 1e-9);
        assertEquals(3,vector.z, 1e-9);
        assertEquals(0,vector.isPoint);
    }

    @Test
    void testAdd() {
        Vector vec=vector.add(vectorAdd);
        assertEquals(2,vec.x, 1e-9);
        assertEquals(3,vec.y, 1e-9);
        assertEquals(4,vec.z, 1e-9);
        assertEquals(0,vec.isPoint);
    }

    @Test
    void testNormalize() {
        Vector vec= vectorAdd.normalize()
        assertEquals(1,vec.getNorme(), 1e-9);
        assertEquals(1/Math.sqrt(3), vec.x, 1e-9);
        assertEquals(1/Math.sqrt(3), vec.y, 1e-9);
        assertEquals(1/Math.sqrt(3), vec.z, 1e-9);
        assertEquals(0,vec.isPoint);
    }

    @Test
    void testDotProduct(){
        assertEquals(6,vector.dotProduct(vectorAdd),1e-9);
    }
}

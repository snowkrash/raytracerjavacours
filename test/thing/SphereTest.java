package thing;

import geometry.Vector;
import org.junit.Test;
import tracer.Intersection;
import tracer.Ray;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SphereTest {

    @Test
    public void intersect() {
        //TEST1-- pleine intersect
        Sphere sphere1=new Sphere(1, Vector.vector(0,3,0));
        Ray ray=new Ray(Vector.point(0,1, 0), Vector.vector(0,1,0));
        Optional<Intersection> optional=sphere1.intersect(ray);

        assertTrue(optional.isPresent());
        assertEquals(1,optional.get().getDistance(),1e-9);

        assertEquals(1,optional.get().getRayon().getDirection().normalize().dotProduct(
                Vector.vector(0,1,0)
        ), 1e-9);

        //TEST2 -- pas d'intersect
        sphere1=new Sphere(1, Vector.vector(2,3,0));
        ray=new Ray(Vector.point(0,1, 0), Vector.vector(0,1,0));
        optional=sphere1.intersect(ray);

        assertFalse(optional.isPresent());

        //TEST3 --on est à l'interieur et décalage à droite
        sphere1=new Sphere(10, Vector.point(2,3,0));
        ray=new Ray(Vector.point(0,0, 0), Vector.vector(0,1,0));
        optional=sphere1.intersect(ray);

        assertTrue(optional.isPresent());
        assertEquals(13,optional.get().getDistance(),0.5); // bon ordre de grandeur ( 1+ 2 +10 sin( un peu moins de pi/2) )
        assertEquals(12,optional.get().getDistance(),0.85);
        assertEquals(1,optional.get().getRayon().getDirection().dotProduct(Vector.vector(0,1,0)),1e-9);

        //TEST4 -- direction de ray en biais

        ray=new Ray(Vector.point(0,0, 0), Vector.vector(5,1,2));
        optional=sphere1.intersect(ray);

        assertTrue(optional.isPresent());

    }
}

package tracer;

import geometry.Vector;
import org.junit.Test;
import viewer.Main;

import static org.junit.Assert.assertEquals;

public class CameraTest {
    Camera camera=new Camera();

    @Test
    public void getDirectionPixel() {
        Vector dir=camera.getDirectionPixel(Main.IMAGE_WIDTH/2, Main.IMAGE_HEIGHT/2);
        assertEquals(0, dir.getZ(),1e-7);
        assertEquals(0, dir.getX(),1e-7);
        assertEquals(1, dir.getY(),1e-7);
        assertEquals(0, dir.getIsPoint());
    }

    @Test
    public void getDirectionPixelnormalized() {
        Vector dir=camera.getDirectionPixelNormalized(Main.IMAGE_WIDTH/2, Main.IMAGE_HEIGHT/2);
        assertEquals(0, dir.getZ(),1e-7);
        assertEquals(0, dir.getX(),1e-7);
        assertEquals(1, dir.getY(),1e-7);
        assertEquals(0, dir.getIsPoint());
        assertEquals(1,dir.getNorme(),1e-9);
        //checking normalized
        Vector dir2=camera.getDirectionPixelNormalized(35,89);
        assertEquals(1,dir2.getNorme(),1e-9);
    }
}
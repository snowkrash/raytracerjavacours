package tracer;

import geometry.Vector;
import javafx.scene.paint.Color;
import org.junit.Test;
import thing.Sphere;

import static org.junit.Assert.*;

public  class RaytracerTest {

  private Raytracer raytracer = new Raytracer(new Scene(new Sphere(0.5, Vector.point(0,10,0))));

  @Test
  public void getColor() {
    assertEquals(Color.BLACK, raytracer.getColor(0,0));
    assertEquals(Color.BLACK, raytracer.getColor(10,0));
    assertEquals(Color.BLACK, raytracer.getColor(0,10));
    assertEquals(Color.BLACK, raytracer.getColor(42,42));
  }
}
